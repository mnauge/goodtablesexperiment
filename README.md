# goodTablesExperiment

Partage d'expérience de nos premiers pas avec goodtables suite au récent lancement de [schema.data.gouv](https://www.data.gouv.fr/fr/posts/lancement-de-schema-data-gouv-fr/)


# Table des matières
* [Expérimentation de Goodtables (document principal modifiable)](https://hackmd.io/mn2dUi7VT1y3fu5nzNp8ZQ?view)


* [Performances de validation d'un tableur de 100 000 lignes (lien direct)](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/PerformanceTest/PerformanceTest.ipynb?flush_cache=true) 
* [Création d'un tableur de données simulé réaliste (lien direct)](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/CreateSampleTable/CreateSampleTable.ipynb?flush_cache=true) 
* [Scrapping web des Formats CINES](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/CinesFacileFormatScrap/Scrap%20Format%20CINES.ipynb?flush_cache=true)

https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fgoodtablesexperiment/7a8d8b1fe7a7f78358ddb80288c8fda2fa3c7c41