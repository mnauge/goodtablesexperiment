# Expérimentation de Goodtables


**GT-Nakala-Interconsortium**


## En Bref
Ce document est l'occasion de partager un retour d'expérience pratique de nos premiers pas avec goodtables suite au lancement de [schema.data.gouv](https://www.data.gouv.fr/fr/posts/lancement-de-schema-data-gouv-fr/) (5 juin 2019)

## Mais goodtables c'est quoi ?
Goodtables permet de vérifier qu'un tableur de données (ou métadonnées) respecte un schéma. 
>"Les schémas permettent entre autres de valider qu’un jeu de données se conforme à un schéma, de générer de la documentation automatiquement, de générer des jeux de données d’exemple ou de proposer des formulaires de saisie standardisés."

## Où est l'intérêt ?
Tout d'abord nous pouvons commencer par rappeler que l'idée de "contrôler/maîtriser" la "validité/qualité" des données n'est pas nouveau. C'est un outillage avant tout conceptuel, méthodologique et **modélisant**. 
C'est en début de projet, lors des phases de conceptions, qu'il est conseillé et important de définir de manière **formelle** dans **schéma** les données que nous souhaitons appréhender. A cette occasion nous devons être très précis et répondre aux questions suivantes : 
Quelle est le caractère facultatif ou le caractère obligatoire de cette donnée ? 
Quelle est la nature, le type de cette donnée ?
Y a-t-il une liste finie de valeurs possibles et/ou un thésaurus sur lequel s'appuyer ? 
Pour les données pouvant prendre une "infinité" de valeurs,  y a-t-il un **motif** à respecter ? 
> Malgrè 20 ans d'expérience [..] dès que je fais une analyse, j'ai toujours un satané problème de consistance avec ces foutus champs dates... *Pr. T.Stamp*

Rappelons nous bien que toute cette rigueur sert un noble but : Permettre l'analyse des données. Précisement, celà est indispensable pour effectuer les opérations suivantes : 
- **Grouper/Filtrer** : Rassembler/Isoler les occurrences qui partagent une propriété  commune
- **Ordonnancer/Trier** : Ajouter aux occurrences une notion d’ordre/antériorité-postérité
- **Compter** : Additionner les occurrences d’un groupe. Mesurer la corrélation de deux variables, etc...
- Croiser/Aligner : Combinaison/Cas particulier de grouper/filtrer 

 
Si ce travail de spécification formelle semble évident et indispensable (bien qu'en réalité complexe et chronophage), autant l'exploiter au maximum et profiter ensuite d'un ensemble d'apparaillage technique !

D'un point de vue pratique et technique, nous retrouvons la mise en exécution du concept de schéma de validation dans tous les systèmes de gestion de base de données (SGBD) sous les étiquettes "contraintes" de validités et d'intégrités, qui mécaniquement empêchent complètement l'entrée en base de valeurs "interdites". 
De manière plus externalisées et légèrement moins strictent nous retrouvons le Xml Schema Definition (XSD) pour les données strucurées en Xml.
Et en écho au XSD mais pour des données structurées en tableaux, nous retrouvons le **schema table** qui nous interessent aujourd'hui. 




## Goodtables et Nakala ?
Pour donner un cadre applicatif pratique en SHS, nous proposons d'intégrer goodtables avant l'utilisation du service Nakala d'Huma-Num permettant le stockage de ressources en SHS. Pour Nakala toutes les ressources doivent être obligatoirement décritent  par un jeux de métadonnées descriptives minimales.

Dans de nombreux projets ce sont plusieurs centaines de ressources qui doivent être téléversés dans Nakala avec en général une dizaine de métadonnées par ressources. Partant de l'idée que les métadonnées descriptives doivent permettre d'effectuer des "recherches" au sens isoler/filtrer toutes les ressources qui partagent une (ou plusieurs) propriétés communes. Il devient impératif d'assurer une réelle "consistance"/régularité des valeurs des métadonnées. 

Voici ci-dessous une chaine de traitement qui fait apparaître goodtables avant un dépôt massif Nakala.

Chaine proposée :

1. Produire un tableur de métadonnées descriptives de ressources (chercheur/archiviste/…)

2. Transformer automatiquement le tableur pour le rendre compatible avec un outil de dépôt massif (formule excell ou openrefine ou script python ou un code php ou autre...)

3. Vérifier la qualité/intégrité des données du tableur avec **goodtables**. 
>Tant que goodtables détecte des erreurs : modifier le tableur de données (étape 1.) et/ou les transformations (étape 2.)

4. Déposer massivement dans Nakala avec un outils (nakalaworkflow ou easy_batch ou nouvelle solution nakala ou script python utilisant l’api ou autre)

5. Moissonner/Intéroger Isidore et/ou Nakala pour vérifier la qualité du dépôt en le comparant avec le tableur validé


## Objectifs de ce document ?
* [Découvrir les manières de lancer l'exécution de goodtables](#howToRun)
* [Identifier d'éventuelles limites et difficultés](#perf)
* [Proposer des blocs de schémaTable réutilisables pour un dépôt Nakala](#schemaBlocNkl)
* [Perspective Frictionless data](#frictionLess)


Pour commencer nos essais nous devons disposer d'un **tableur** de données à vérifier et de son schéma de validation (**schémaTable**)

## Corpus A :  Liste annuelle des prénoms des nouveaux-nés déclarés à l'état-civil

Pour expérimenter l'outil goodtables, nous commençons par l'utiliser avec les fichiers produits par Charles Nepote

https://github.com/CharlesNepote/liste-prenoms-nouveaux-nes 

* **tableur** (avec erreurs à détecter) : [prenoms-nouveaux-nes.exemple.invalide.1.1.csv](https://github.com/CharlesNepote/liste-prenoms-nouveaux-nes/blob/master/prenoms-nouveaux-nes.exemple.invalide.1.1.csv)
* **schémaTable** (contenant les "contraintes d'intégrités" de chaque colonne) : [prenom-schema.json](https://github.com/CharlesNepote/liste-prenoms-nouveaux-nes/blob/master/prenom-schema.json)


Pour lancer l'exécution de goodtables sur ces fichiers d'entrées, nous expérimentons plusieurs méthodes.

## Les méthodes de lancement d'exécution de goodtables <a id="howToRun"></a>

### Méthode 1 : Service web goodtables.io <a id="method4"></a>
TODO
Nécessite de déposer ses fichiers à vérifier sur github et d'ouvrir des droits à gootdtables :-\

Après recherche j'ai trouvé une alternative :-) 
http://try.goodtables.io

### Méthode 2 : CLI (Command Line Interface)<a id="method1"></a>
Lancement en ligne de commande du programme compilé (sous windows), disponible après avoir réalisé une installation avec *pip install goodtables*. 

Voici ci-dessous la syntaxe pour lancer goodtables en ligne de commande.

<b>Syntaxe :</b>
```
goodtables.exe --schema monFichierSchema.json monfichierTableurDonnees.csv
```
Pour notre essai nous devons remplacer *monFichierSchema.json* par *prenom-schema.json* ainsi que *monfichierTableurDonnees.csv* par *prenoms-nouveaux-nes.exemple.invalide.1.1.csv*.
De plus dans notre cas l'executable goodtables.exe est dans le dossier *C:\ProgramData\Anaconda3\Scripts\\*, nous pouvons ajouter ce chemin avant *goodtables.exe*  

<b>Ligne de commande executée:</b>
```
C:\ProgramData\Anaconda3\Scripts\goodtables.exe --schema C:\Users\mnauge\Documents\Work\goodtables\exemple_nomnaissance\prenom-schema.json C:\Users\mnauge\Documents\Work\goodtables\exemple_nomnaissance\prenoms-nouveaux-nes.exemple.invalide.1.1.csv

```
<b>Résultat :</b>
```
>>
DATASET
=======
{'error-count': 7,
 'preset': 'nested',
 'table-count': 1,
 'time': 0.125,
 'valid': False}

TABLE [1]
=========
{'encoding': 'utf-8',
 'error-count': 7,
 'format': 'csv',
 'headers': ['COMMUNE_NOM',
             'COLL_INSEE',
             'ENFANT_SEXE',
             'ENFANT_PRENOM',
             'NOMBRE_OCCURRENCES',
             'ANNEE'],
 'row-count': 17,
 'schema': 'table-schema',
 'scheme': 'file',
 'source': 'C:\\Users\\mnauge\\Documents\\Work\\goodtables\\exemple_nomnaissance\\prenoms-nouveaux-nes.exemple.invalide.1.1.csv',
 'time': 0.031,
 'valid': False}
---------
[2,2] [pattern-constraint] The value "3238" in row 2 and column 2 does not conform to the pattern constraint of "^([013-9]\d|2[AB1-9])\d{3}$"
[2,4] [pattern-constraint] The value "Ad am" in row 2 and column 4 does not conform to the pattern constraint of "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*('|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç']*){1,2}){1,3}|)$"
[3,1] [pattern-constraint] The value "RENNES" in row 3 and column 1 does not conform to the pattern constraint of "^(Le |La |Les |Los |Aux |L'|)([A-ZÉÇŒÈÎ])(((-| | - |')[A-ZÉÇŒÈÎ])|('|-| |)[a-zàâéèêëïîÿôûüœç])*( \([A-Z][a-z]*\)|)$"
[3,4] [pattern-constraint] The value "AlexAndre" in row 3 and column 4 does not conform to the pattern constraint of "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*('|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç']*){1,2}){1,3}|)$"
[4,1] [pattern-constraint] The value "rennes" in row 4 and column 1 does not conform to the pattern constraint of "^(Le |La |Les |Los |Aux |L'|)([A-ZÉÇŒÈÎ])(((-| | - |')[A-ZÉÇŒÈÎ])|('|-| |)[a-zàâéèêëïîÿôûüœç])*( \([A-Z][a-z]*\)|)$"
[4,3] [pattern-constraint] The value "H" in row 4 and column 3 does not conform to the pattern constraint of "^(M|F|I)$"
[7,4] [pattern-constraint] The value "AssiaAmélie" in row 7 and column 4 does not conform to the pattern constraint of "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*('|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç']*){1,2}){1,3}|)$"

```

A la lecture du résultat produit par goodtables nous pouvons voir qu'il a détecté 7 erreurs dans notre tableur de données. La vérification c'est effectuée en 0.125 secondes. Chaque cellule contenant une erreur est identifiée par un index numéro de ligne, numéro de colonne.

Remarque : la sortie n'est pas aussi sexy que les images du site goodtables.io, pour ce genre de rendu il faut utiliser le service web décrit en [Méthode 4](#method4)


### Méthode 3 : Code Python <a id="method2"></a>
Lancement via un code python et son interpreteur.


<b>Code python :</b>

```python
from goodtables import validate

#appel de la fonction validate de la librairie goodtables
report = validate('prenoms-nouveaux-nes.exemple.invalide.1.1.csv',schema='prenom-schema.json')
#afficher le resultat de la vérification de goodtables
print(report)
```

```
{
'time': 0.109, 
'valid': False,
'error-count': 7,
'table-count': 1,
'tables': [{'time': 0.0, 'valid': False, 'error-count': 7, 'row-count': 17, 
'source': 'prenoms-nouveaux-nes.exemple.invalide.1.1.csv', 
'headers': ['COMMUNE_NOM', 'COLL_INSEE', 'ENFANT_SEXE', 'ENFANT_PRENOM', 'NOMBRE_OCCURRENCES', 'ANNEE'], 
'scheme': 'file', 
'format': 'csv', 
'encoding': 'utf-8', '
schema': 'table-schema', 

'errors': [
{'code': 'pattern-constraint', 'row-number': 2, 'column-number': 2, 'message': 'The value "3238" in row 2 and column 2 does not conform to the pattern constraint of "^([013-9]\\d|2[AB1-9])\\d{3}$"', 'message-data': {'value': '3238', 'constraint': '^([013-9]\\d|2[AB1-9])\\d{3}$'}}, 
{'code': 'pattern-constraint', 'row-number': 2, 'column-number': 4, 'message': 'The value "Ad am" in row 2 and column 4 does not conform to the pattern constraint of "^\'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*(\'|((\'[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç\']*){1,2}){1,3}|)$"', 'message-data': {'value': 'Ad am', 'constraint': "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*('|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç']*){1,2}){1,3}|)$"}}, 
{'code': 'pattern-constraint', 'row-number': 3, 'column-number': 1, 'message': 'The value "RENNES" in row 3 and column 1 does not conform to the pattern constraint of "^(Le |La |Les |Los |Aux |L\'|)([A-ZÉÇŒÈÎ])(((-| | - |\')[A-ZÉÇŒÈÎ])|(\'|-| |)[a-zàâéèêëïîÿôûüœç])*( \\([A-Z][a-z]*\\)|)$"', 'message-data': {'value': 'RENNES', 'constraint': "^(Le |La |Les |Los |Aux |L'|)([A-ZÉÇŒÈÎ])(((-| | - |')[A-ZÉÇŒÈÎ])|('|-| |)[a-zàâéèêëïîÿôûüœç])*( \\([A-Z][a-z]*\\)|)$"}}, 
{'code': 'pattern-constraint', 'row-number': 3, 'column-number': 4, 'message': 'The value "AlexAndre" in row 3 and column 4 does not conform to the pattern constraint of "^\'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*(\'|((\'[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç\']*){1,2}){1,3}|)$"', 'message-data': {'value': 'AlexAndre', 'constraint': "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*('|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç']*){1,2}){1,3}|)$"}}, 
{'code': 'pattern-constraint', 'row-number': 4, 'column-number': 1, 'message': 'The value "rennes" in row 4 and column 1 does not conform to the pattern constraint of "^(Le |La |Les |Los |Aux |L\'|)([A-ZÉÇŒÈÎ])(((-| | - |\')[A-ZÉÇŒÈÎ])|(\'|-| |)[a-zàâéèêëïîÿôûüœç])*( \\([A-Z][a-z]*\\)|)$"', 'message-data': {'value': 'rennes', 'constraint': "^(Le |La |Les |Los |Aux |L'|)([A-ZÉÇŒÈÎ])(((-| | - |')[A-ZÉÇŒÈÎ])|('|-| |)[a-zàâéèêëïîÿôûüœç])*( \\([A-Z][a-z]*\\)|)$"}}, 
{'code': 'pattern-constraint', 'row-number': 4, 'column-number': 3, 'message': 'The value "H" in row 4 and column 3 does not conform to the pattern constraint of "^(M|F|I)$"', 'message-data': {'value': 'H', 'constraint': '^(M|F|I)$'}}, 
{'code': 'pattern-constraint', 'row-number': 7, 'column-number': 4, 'message': 'The value "AssiaAmélie" in row 7 and column 4 does not conform to the pattern constraint of "^\'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*(\'|((\'[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç\']*){1,2}){1,3}|)$"', 'message-data': {'value': 'AssiaAmélie', 'constraint': "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*('|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç]*){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇ][a-zéàèùäëïüöÿâêîôûŷç']*){1,2}){1,3}|)$"}}]}], 
'warnings': [], 'preset': 'table'}
```

Nous retrouvons globalement les mêmes informations qu'en CLI. Nous pouvons noter une petite différence de temps d'execution (0.109 secondes contre 0.125 en CLI).
Il faudrait relancer des centaines d'executions avec des fichiers plus volumineux pour réellement pouvoir commencer à comprarer les performances CLI vs Interpreteur Python...


### Méthode 4 : Code Python dans un document computationnel avec execution myBinder <a id="method3"></a>
Le document computationnel permettant d'avoir dans un même document des zones de textes éditorialisées et du code exécutable le tout accessible en ligne sans installation pour le lecteur semble très prometteur.
C'est pourquoi nous décidons de tenter l'expérience de créer un document computationnel pour lancer l'exécution d'une vérification de tableur.
Nous avons créé un document computationnel, versionné sur le gitlab d'Huma-Num, puis nous pouvons lancer sons exécution avec le service web [myBinder](https://gke.mybinder.org/).

Voici le [document computationnel](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fgoodtablesexperiment/005506725a39697276515970aec72333435b4802?filepath=RunGoodtablesOverNotebook) exécuté par myBinder.

Le résultat est plutôt prometteur. La librairie est bien installé et fonctionnelle avec des temps d'exécution légèrement inférieurs aux résultats obtenus sur machine personnelle.

Il est donc tout à fait envisageable de produire des documents computationnels d'analyse de données utilisant goodtables en début de fichier pour justifier la qualité/propreté des données d'entrées avant de détailler les analyses qui suiveront.

Idée/Concept/Méthode : 
Faire un article scientifique qui : 

1- Rends accessible un tableur de données (peut être issu de requêtes sur API d'entrepôt de données ou via XQuery pour les projets utilisant XML comme élément structurant de leurs jeux de données). Evidement mis sur un service de en capacité de faire de la gestion de version type Git, Zenodo, etc... 

2- Lance une vérification de la propreté du tableur et ayant mis à disposion un schemaTable.

3- Réalise des opérations d'analyse de données

Avec cette méthode les données d'entrées d'analyse sont accessibles, décrites (entre par le schemaTable), validés de leur consistance et donc plus à même de faire des analyses de confiances.






--- 
# Identifier d'éventuelles limites et difficultés<a id="perf"></a>

En vue d'une utilisation concrète de goodtables sur nos corpus nous proposons de tester sa "performance" pour un tableur de grande taille.

## Performances de goodtables pour un tableur de données contenant 100 000 lignes

Pour réaliser ce test de performance nous avons réalisé un document computationnel dédié pour traiter particulièrement de cette question : [PerformanceTest](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/PerformanceTest/PerformanceTest.ipynb?flush_cache=true) 

Dans ce document nous effectuons une analyse du temps d'exécution en fonction du nombre de lignes du tableur de données à vérifier.

Pour disposer de tableurs de données de taille différentes nous avons également réalisé un document computationnel : [CreateSampleTable](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/CreateSampleTable/CreateSampleTable.ipynb?flush_cache=true) pour générer des tableurs de données simulées mais réalistes.

#### Extraits de résultats du document [PerformanceTest](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/PerformanceTest/PerformanceTest.ipynb?flush_cache=true) 


nb. lignes | Temps (secondes)
--- | --- | ---
10 | 0.104895
100 | 0.107211
1000 | 0.424158
10000 | 3.608842
100000 | 36.450316

![plot time vs nb lines](https://gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/599d7a7b5ef388c97875eb59e72760cd600b9bab/PerformanceTest/plotNbLinesVsTime.PNG?inline=false)

#### Conclusion de l'étude de performance sur tableur simulé
Goodtables semble pouvoir valider de gros tableurs dans une contrainte de temps acceptable inférieur à la minute sur une machine de bureau, moyennement quelques configurations tels que **row_limit** et **error_limit**.

Goodtables offre un mécanisme de validation fiable et utilisable, permettant des vérifications faciles et externalisées du producteur de données.

Ce type de vérification pourrait devenir une étape indispensable avant toute analyse quantitative pour s'assurer d'étudier des données "propres".



---
## Définition d'un schéma réutilisable avant dépôt Nakala
Vous l'aurez compris une étape indispensable aux contrôles qualités automatisés est la création de [schémaTable](https://frictionlessdata.io/specs/table-schema/)

Un schéma complet générique réutilisable est probalement impossible à créer car chaque projet/corpus comporte son lot de spécificités. Cependant nous pouvons identifier des **blocs de schéma** ré-utilisables dans les nombreux projets utilisant Nakala.

Exemples de colonnes pour un dépôt Nakala 

* title : obligatoire, unique se rapprochant d'un système de cote. En général de la forme  : 
> codeArchive_codeAuteur_numeroCarton_numeroClasseur_numeroPage

* creator : obligatoire, doit contenir un nom, prénom (ISO XX?) Regex à écrire... s'inspirer de celle de Charles Nepote ? Attention les yeux ce n'est pas une blague: 
```xml
"pattern": "^'?[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇŒÆ][a-zéàèùäëïüöÿâêîôûŷçæœ]*(|'|(('[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇŒÆa-zéàèùäëïüöÿâêîôûŷçæœ][a-zéàèùäëïüöÿâêîôûŷçæœ]*'?){1,2}|(-[A-ZÉÀÈÙÄËÏÖÜŸÂÊÎÔÛŶÇŒÆ][a-zéàèùäëïüöÿâêîôûŷçæœ]*'?){1,2}){1,5})$"
```

* nakala format : obligatoire, doit contenir une valeur d'une liste finie (à jour) proposée par le CINES. Pour générer cette expression régulière nous avons produit un script dédié : cf [Cines Facile Format Scrap](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/goodtablesexperiment/raw/master/CinesFacileFormatScrap/Scrap%20Format%20CINES.ipynb?flush_cache=true)


```
"pattern": "^(AAC AAC|AIFF PCM|APNG|DAE UTF-8 1\\.4\\.1|FLAC FLAC 1\\.2\\.1|GIF 87a|GIF 89a|GeoTIFF|HDF5 1\\.0|HDF5 2\\.0|JPEG|JPEG2000|JPEG 1\\.00|JPEG 1\\.01|JPEG 1\\.02|MKV AVC/FLAC|MPEG-4 AAC|MPEG-4 AVC|MPEG-4 AVC/AAC|MPEG-4 AVC/AAC LC|ODS 1\\.0|ODS 1\\.1|ODS 1\\.2|ODT 1\\.0|ODT 1\\.1|ODT 1\\.2|OGG Theora/Vorbis|OGG Vorbis|PDFA 1a|PDFA 1b|PDFA 2a|PDFA 2u|PDFA 2b|PDFA 3a|PDF 1\\.0|PDF 1\\.1|PDF 1\\.2|PDF 1\\.3|PDF 1\\.4|PDF 1\\.5|PDF 1\\.6|PDF 1\\.7|PLY ASCII 1\\.0|PNG 1\\.0|PNG 1\\.1|PNG 1\\.2|SIARD|SVG UTF-8 1\\.0|SVG UTF-8 1\\.1|SVG UTF-8 1\\.2|TEI UTF-8 3\\.0\\.0|TEI UTF-8 3\\.1\\.0|TIFF|TIFF 4\\.0|TIFF 5\\.0|TIFF 6\\.0|TXT UTF-8|WAV|XDMF|XML UTF-8 1\\.0|XSD)$"
```

* nakala type : obligatoire, doit contenir une valeur d'une liste finie Regex à écrire... 
```
"pattern": "^(Text|Image|)$"
```

* nakala project Handle : obligatoire. que des chiffres une barre oblique que des chiffres Regex à écrire... 

* date created : obligatoire, une date de la forme YYYY-MM-DD 
Pour valider une date de la forme YYYY-MM-DD avec les contraintes suivantes : 
YYYY entre 0000 et 9999
MM entre 01 et 12
DD entre 01 et 31
Nous pouvons écrire nous même l'expression régulière suivante:

```xml
"pattern": "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$"

```
Mais il est aussi possible et conseillé de definir un type date à la place de string dans le **schémaTable**.
```xml
"type": "date",

```
Cependant l'écriture d'une expression régulière de date doit être envisagée pour les dates avec empans...



### date avec empan :
YYYY-MM-DD
YYYY-MM
YYYY
YYYY-MM-DD/YYYY-MM-DD
YYYY-MM/YYYY-MM
YYYY/YYYY

rarement:
YYYY-MM-DD|YYYY-MM-DD
YYYY-MM-DD|YYYY
YYYY-MM-DD|YYYY-MM-DD/YYYY-MM-DD
YYYY-MM|YYYY-MM-DD/YYYY-MM-DD

Le / indique un espace temporel entre deux dates
Le | indique deux dates différentes : exemple un surtirage d'une revue (un encart)

TODO : en fait il existe une quantité explosive de combinaisons, idée : coder une boucle en python sur les motif pour générer la regex ^^




### Résumé
Tous les projets de dépôt doivent avoir au moins des colonnes bien indentifiées minimales, tels que title, created, creator, dataType, dataFormat... Puis stabiliser les valeurs possibles de ces colonnes semble indispensable pour des recherches croisées.

Bien que Nakala fasse des vérifications en cours de dépôt il peut être utile d'en faire en amont pour corriger au plus tôt ses tableurs et ses outils de transformations... afin de minimiser les opérations de mise à jour distantes.

Par ce mécanisme nous aurions en service externe la validation Facile pour les données/ressources et goodtables pour valider les métadonnées....

Evidemment, laisser souple Nakala est une bonne idée, mais ajouter pour ceux qui le souhaite une validation externe pour obtenir un "badge quality checked" externalisé semble cool :-p





## Perspective Frictionless data
L'utilisation de goodtables nous invite plus globalement à l'utilisation de [data package](http://frictionlessdata.io/data-packages/) et de manière plus générale à la vision [Frictionless Data](http://frictionlessdata.io).

Pour ce faire idée de cette vision, voici une vidéo explicative de quelques minutes : 
<a href="https://www.youtube.com/watch?v=lWHKVXxuci0" target="_blank"><img src="http://img.youtube.com/vi/lWHKVXxuci0/0.jpg" 
alt="miniature video friction less data" width="240" height="180" border="10" /></a>


## Annexe : Description des machines de test
### Machine 1
PC portable Dell XPS
Intel core i7 7Y75 
64 bits 
Ram 16 Go 
Win 10

### Machine 2
Mac Mini
8 coeurs
16 Go de ram

# Liens utiles
* [Frictionless data : Table schema](https://frictionlessdata.io/docs/table-schema/)
* [Validating scraped data using goodtables](https://okfnlabs.org/blog/2017/11/29/validating-scraped-data-using-goodtables.html)

* [Marché public](https://git.opendatafrance.net/scdl/marches-publics/blob/v1.0/schemas/marche_public_SCDL.json)

# Appel à participation <a id=call></a>


Laurent/Adrien/Yannick : Avez-vous déjà écrit des expressions régulières pour des vérifications côté serveur ?


Adeline/Nicolas : Côté projets européens il me semble qu’il y a déjà des projets sur la qualité des données et des mécanismes de vérification, ça vous dit quelque chose ? Avez-vous des liens/contacts à partager ?


Stéphane /Aurelia/Fabrice/Bruno/… : Avez-vous un gros tableur problématique à partager pour lancer des tests en dehors de vos machines ?

 
? : Relire/corriger rendre tout ça plus digeste et intelligible?!

# Remerciements
Ce document a été réalisé de manière collaborative par le GT-Nakala-Interconsortium avec, entre autres, les auteurs suivants :
* Michael Nauge (Laboratoire FoReLLIS/MIMMOC : Université de Poitiers, Consortium CAHIER)
* Aurelia Vasile (MSH Clermont)
* Stéphane Pouyllau  (TGIR Huma-Num)
* Fatiha Idmhand (Institut des Textes et Manuscrits Modernes-UMR8132, Université de Poitiers, Consortium CAHIER)
* ....
* ....
* ....
* ....
